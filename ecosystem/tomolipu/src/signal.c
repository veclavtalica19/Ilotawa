#include <iso646.h>
#include <stdbool.h>

#include <tomolipu/include/signal.h>
#include <tomolipu/include/testing.h>
#include <tomolipu/include/types/buffer.h>

static it_buffer clean_signal_vector;

bool it_signal_register(enum it_signal const mask, it_signal_prototype *const callback) {
    it_test(callback != NULL);
    it_test(mask > 0 and mask < it_signal_limit);

    if (!it_buffer_is_initialized(&clean_signal_vector))
        clean_signal_vector = it_buffer_init(it_typeinfo_init(it_signal_prototype *));

    if (mask and it_signal_clean)
        if (!it_buffer_push(&clean_signal_vector, &callback))
            goto signal_error;

    return true;

signal_error:
    return false;
}

// todo: Deal with errors and their propagation behavior.
void it_signals_invoke(enum it_signal const mask) {
    it_test(mask > 0 and mask < it_signal_limit);

    if (mask and it_signal_clean and it_buffer_is_initialized(&clean_signal_vector))
        for (it_buffer_iterator it
             = it_buffer_iterator_init(&clean_signal_vector, it_flag_access_readable);
             it_buffer_iterator_pump(&it);)
            ((it_signal_prototype *)it.current)();
}
