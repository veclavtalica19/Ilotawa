#include <iso646.h>
#include <stdbool.h>

#include <tomolipu/include/testing.h>
#include <tomolipu/include/types/buffer.h>
#include <tomolipu/include/types/string.h>

it_string it_string_from_utf8(it_buffer utf8_buffer) {
    it_string result = { .buffer = utf8_buffer };

    // Dry-fire iterator to check well-formity of utf8 buffer.
    for (it_string_iterator it = it_string_iterator_init(&result); it_string_iterator_pump(&it);) {
    }

    return result;
}

// todo: uint32_t it_string_get(it_string const* string, uint32_t index);

bool it_string_check_equality(it_string const *restrict left, it_string const *restrict right) {
    if (left->buffer.private.length != right->buffer.private.length)
        return false;

    it_buffer left_view = left->buffer;
    it_buffer right_view = right->buffer;

    // https://en.wikipedia.org/wiki/Byte_order_mark
    if (left->buffer.private.length >= 3)
        if (((unsigned char *)left->buffer.private.data)[0] == 0xEF
            and ((unsigned char *)left->buffer.private.data)[1] == 0xBB
            and ((unsigned char *)left->buffer.private.data)[2] == 0xBF)
            left_view = it_buffer_make_subview(
                &left->buffer, 3, left->buffer.private.length - 3, it_flag_access_readable);

    if (right->buffer.private.length >= 3)
        if (((unsigned char *)right->buffer.private.data)[0] == 0xEF
            and ((unsigned char *)right->buffer.private.data)[1] == 0xBB
            and ((unsigned char *)right->buffer.private.data)[2] == 0xBF)
            right_view = it_buffer_make_subview(
                &right->buffer, 3, right->buffer.private.length - 3, it_flag_access_readable);

    return it_buffer_check_equality(&left_view, &right_view);
}

void it_string_free(it_string *item) { it_buffer_free(&item->buffer); }

it_string_iterator it_string_iterator_init(it_string const *string) {
    it_test(string != NULL);

    unsigned char *current_byte = string->buffer.private.data;

    if (string->buffer.private.length >= 3)
        // https://en.wikipedia.org/wiki/Byte_order_mark
        if (((unsigned char *)string->buffer.private.data)[0] == 0xEF
            and ((unsigned char *)string->buffer.private.data)[1] == 0xBB
            and ((unsigned char *)string->buffer.private.data)[2] == 0xBF)
            current_byte += 3;

    return (it_string_iterator) {
        .current_byte = current_byte,
        .end_byte = &((unsigned char *)string->buffer.private.data)[string->buffer.private.length],
    };
}

// https://en.wikipedia.org/wiki/UTF-8
bool it_string_iterator_pump(it_string_iterator *it) {
    it_test(it != NULL);

    ptrdiff_t left = it->end_byte - it->current_byte;
    it_test(left >= 0);

    if (left == 0)
        return false;

    it_test((*it->current_byte bitand 0xC0) != 0x80);

    if ((*it->current_byte bitand 0x80) == 0x00) {
        it->current = (uint32_t)it->current_byte[0];
        it->current_byte += 1;

    } else if ((*it->current_byte bitand 0xE0) == 0xC0) {
        it_test(left >= 2);
        it->current = ((uint32_t)(it->current_byte[0] bitand 0x1F) << 6)
            bitor (uint32_t)(it->current_byte[1] bitand 0x3F);
        it->current_byte += 2;

    } else if ((*it->current_byte bitand 0xF0) == 0xE0) {
        it_test(left >= 3);
        it->current = ((uint32_t)(it->current_byte[0] bitand 0x0F) << 12)
            bitor ((uint32_t)(it->current_byte[1] bitand 0x3F) << 6)
            bitor (uint32_t)(it->current_byte[2] bitand 0x3F);
        it->current_byte += 3;

    } else if ((*it->current_byte bitand 0xF8) == 0xF0) {
        it_test(left >= 4);
        it->current = ((uint32_t)(it->current_byte[0] bitand 0x07) << 18)
            bitor ((uint32_t)(it->current_byte[1] bitand 0x3F) << 12)
            bitor ((uint32_t)(it->current_byte[2] bitand 0x3F) << 6)
            bitor (uint32_t)(it->current_byte[3] bitand 0x3F);
        it->current_byte += 4;
    }

    return true;
}
