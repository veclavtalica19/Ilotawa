/**
 * @brief      Signals for extendable communication for common events.
 *
 * @author     Veclav Talica
 * @date       2023
 *
 */

#pragma once

#include <stdbool.h>

#include <tomolipu/include/compiler.h>

enum IT_FLAG_ENUMERATOR it_signal {
    it_signal_clean = 1 << 0,
    it_signal_limit = 1 << 1,
};

typedef bool it_signal_prototype(void);

IT_API bool it_signal_register(enum it_signal mask, it_signal_prototype *callback);

IT_API void it_signals_invoke(enum it_signal mask);
