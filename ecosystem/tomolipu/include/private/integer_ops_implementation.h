#pragma once

// https://wiki.sei.cmu.edu/confluence/display/c/INT30-C.+Ensure+that+unsigned+integer+operations+do+not+wrap
// https://wiki.sei.cmu.edu/confluence/display/c/INT32-C.+Ensure+that+operations+on+signed+integers+do+not+result+in+overflow

#include <stdint.h>

#if defined(IT_INTEGER_OPS_COMPILE)

#define it_integer_add_implement_for_signed_type(p_type, p_min, p_max)                             \
    IT_INTEGER_OPS_ATTRIBUTES p_type it_##p_type##_add(p_type a, p_type b);                        \
    IT_INTEGER_OPS_ATTRIBUTES p_type it_##p_type##_add(p_type a, p_type b) {                       \
        if (((b > 0) and (a > (p_max - b))) or ((b < 0) and (a < (p_min - b))))                    \
            it_integer_ops_trap_(false);                                                           \
        return a + b;                                                                              \
    }

#define it_integer_add_sat_implement_for_signed_type(p_type, p_min, p_max)                         \
    IT_INTEGER_OPS_ATTRIBUTES p_type it_##p_type##_add_sat(p_type a, p_type b);                    \
    IT_INTEGER_OPS_ATTRIBUTES p_type it_##p_type##_add_sat(p_type a, p_type b) {                   \
        if ((b > 0) and (a > (p_max - b)))                                                         \
            return p_max;                                                                          \
        if ((b < 0) and (a < (p_min - b)))                                                         \
            return p_min;                                                                          \
        return a + b;                                                                              \
    }

#define it_integer_sub_implement_for_signed_type(p_type, p_min, p_max)                             \
    IT_INTEGER_OPS_ATTRIBUTES p_type it_##p_type##_sub(p_type a, p_type b);                        \
    IT_INTEGER_OPS_ATTRIBUTES p_type it_##p_type##_sub(p_type a, p_type b) {                       \
        if ((b > 0 and a < p_min + b) or (b < 0 and a > p_max + b))                                \
            it_integer_ops_trap_(false);                                                           \
        return a - b;                                                                              \
    }

#define it_integer_mul_implement_for_signed_type(p_type, p_min, p_max)                             \
    IT_INTEGER_OPS_ATTRIBUTES p_type it_##p_type##_mul(p_type a, p_type b);                        \
    IT_INTEGER_OPS_ATTRIBUTES p_type it_##p_type##_mul(p_type a, p_type b) {                       \
        if (a > 0) {                                                                               \
            if (b > 0) {                                                                           \
                if (a > (p_max / b))                                                               \
                    it_integer_ops_trap_(false);                                                   \
            } else if (b < (p_min / a))                                                            \
                it_integer_ops_trap_(false);                                                       \
        } else {                                                                                   \
            if (b > 0) {                                                                           \
                if (a < (p_min / b))                                                               \
                    it_integer_ops_trap_(false);                                                   \
            } else if ((a != 0) && (b < (p_max / a)))                                              \
                it_integer_ops_trap_(false);                                                       \
        }                                                                                          \
        return a * b;                                                                              \
    }

#define it_integer_div_implement_for_signed_type(p_type, p_min, p_max)                             \
    IT_INTEGER_OPS_ATTRIBUTES p_type it_##p_type##_div(p_type a, p_type b);                        \
    IT_INTEGER_OPS_ATTRIBUTES p_type it_##p_type##_div(p_type a, p_type b) {                       \
        if ((b == 0) or ((a == p_min) and (b == -1)))                                              \
            it_integer_ops_trap_(false);                                                           \
        return a / b;                                                                              \
    }

#define it_integer_add_implement_for_unsigned_type(p_type, p_max)                                  \
    IT_INTEGER_OPS_ATTRIBUTES p_type it_##p_type##_add(p_type a, p_type b);                        \
    IT_INTEGER_OPS_ATTRIBUTES p_type it_##p_type##_add(p_type a, p_type b) {                       \
        if (p_max - a < b)                                                                         \
            it_integer_ops_trap_(false);                                                           \
        return a + b;                                                                              \
    }

#define it_integer_add_sat_implement_for_unsigned_type(p_type, p_max)                              \
    IT_INTEGER_OPS_ATTRIBUTES p_type it_##p_type##_add_sat(p_type a, p_type b);                    \
    IT_INTEGER_OPS_ATTRIBUTES p_type it_##p_type##_add_sat(p_type a, p_type b) {                   \
        if (p_max - a < b)                                                                         \
            return p_max;                                                                          \
        return a + b;                                                                              \
    }

#define it_integer_sub_implement_for_unsigned_type(p_type, p_max)                                  \
    IT_INTEGER_OPS_ATTRIBUTES p_type it_##p_type##_sub(p_type a, p_type b);                        \
    IT_INTEGER_OPS_ATTRIBUTES p_type it_##p_type##_sub(p_type a, p_type b) {                       \
        if (a < b)                                                                                 \
            it_integer_ops_trap_(false);                                                           \
        return a - b;                                                                              \
    }

#define it_integer_mul_implement_for_unsigned_type(p_type, p_max)                                  \
    IT_INTEGER_OPS_ATTRIBUTES p_type it_##p_type##_mul(p_type a, p_type b);                        \
    IT_INTEGER_OPS_ATTRIBUTES p_type it_##p_type##_mul(p_type a, p_type b) {                       \
        if ((b != 0) and (a > (p_max / b)))                                                        \
            it_integer_ops_trap_(false);                                                           \
        return a * b;                                                                              \
    }

#define it_integer_div_implement_for_unsigned_type(p_type, p_max)                                  \
    IT_INTEGER_OPS_ATTRIBUTES p_type it_##p_type##_div(p_type a, p_type b);                        \
    IT_INTEGER_OPS_ATTRIBUTES p_type it_##p_type##_div(p_type a, p_type b) {                       \
        if (b == 0)                                                                                \
            it_integer_ops_trap_(false);                                                           \
        return a / b;                                                                              \
    }

#define it_integer_clamp_implement_for_type(p_type)                                                \
    IT_INTEGER_OPS_ATTRIBUTES p_type it_##p_type##_clamp(p_type value, p_type low, p_type high);   \
    IT_INTEGER_OPS_ATTRIBUTES p_type it_##p_type##_clamp(p_type value, p_type low, p_type high) {  \
        if (value < low)                                                                           \
            value = low;                                                                           \
        if (value > high)                                                                          \
            value = high;                                                                          \
        return value;                                                                              \
    }

#define it_integer_convert_implement_from_type_to_type(                                            \
    type_from, type_from_min, type_from_max, type_to, type_to_min, type_to_max)                    \
    IT_INTEGER_OPS_ATTRIBUTES type_to it_##type_to##_from_##type_from(type_from value);            \
    IT_INTEGER_OPS_ATTRIBUTES type_to it_##type_to##_from_##type_from(type_from value) {           \
        if (type_to_min == 0 and type_from_min == 0) {                                             \
            /* Target is unsigned, input is unsigned */                                            \
            it_integer_ops_trap_((uintmax_t)value <= (uintmax_t)type_to_max);                      \
        } else if (type_to_min != 0 and type_from_min == 0) {                                      \
            /* Target is signed, input is unsigned */                                              \
            it_integer_ops_trap_((uintmax_t)value <= (uintmax_t)type_to_max);                      \
        } else if (type_to_min == 0 and type_from_min != 0) {                                      \
            /* Target is unsigned, input is signed */                                              \
            it_integer_ops_trap_(value >= 0);                                                      \
            it_integer_ops_trap_((uintmax_t)value <= (uintmax_t)type_to_max);                      \
        } else {                                                                                   \
            /* Target is signed, input is signed */                                                \
            it_integer_ops_trap_((intmax_t)value <= (intmax_t)type_to_max);                        \
            it_integer_ops_trap_((intmax_t)value >= (intmax_t)type_to_min);                        \
        }                                                                                          \
        return (type_to)value;                                                                     \
    }

#else

#define it_integer_add_implement_for_signed_type(p_type, p_min, p_max)                             \
    IT_API IT_PURE p_type it_##p_type##_add(p_type a, p_type b);

#define it_integer_add_sat_implement_for_signed_type(p_type, p_min, p_max)                         \
    IT_API IT_PURE p_type it_##p_type##_add_sat(p_type a, p_type b);

#define it_integer_sub_implement_for_signed_type(p_type, p_min, p_max)                             \
    IT_API IT_PURE p_type it_##p_type##_sub(p_type a, p_type b);

#define it_integer_mul_implement_for_signed_type(p_type, p_min, p_max)                             \
    IT_API IT_PURE p_type it_##p_type##_mul(p_type a, p_type b);

#define it_integer_div_implement_for_signed_type(p_type, p_min, p_max)                             \
    IT_API IT_PURE p_type it_##p_type##_div(p_type a, p_type b);

#define it_integer_add_implement_for_unsigned_type(p_type, p_max)                                  \
    IT_API IT_PURE p_type it_##p_type##_add(p_type a, p_type b);

#define it_integer_add_sat_implement_for_unsigned_type(p_type, p_max)                              \
    IT_API IT_PURE p_type it_##p_type##_add_sat(p_type a, p_type b);

#define it_integer_sub_implement_for_unsigned_type(p_type, p_max)                                  \
    IT_API IT_PURE p_type it_##p_type##_sub(p_type a, p_type b);

#define it_integer_mul_implement_for_unsigned_type(p_type, p_max)                                  \
    IT_API IT_PURE p_type it_##p_type##_mul(p_type a, p_type b);

#define it_integer_div_implement_for_unsigned_type(p_type, p_max)                                  \
    IT_API IT_PURE p_type it_##p_type##_div(p_type a, p_type b);

#define it_integer_clamp_implement_for_type(p_type)                                                \
    IT_API IT_PURE p_type it_##p_type##_clamp(p_type value, p_type low, p_type high);

#define it_integer_convert_implement_from_type_to_type(                                            \
    p_type_from, p_type_from_min, p_type_from_max, p_type_to, p_type_to_min, p_type_to_max)        \
    IT_API IT_PURE p_type_to it_##p_type_to##_from_##p_type_from(p_type_from value);

#endif // defined(IT_INTEGER_OPS_COMPILE)

// clang-format off

it_integer_add_implement_for_signed_type(int8_t, INT8_MIN, INT8_MAX)
it_integer_add_implement_for_signed_type(int16_t, INT16_MIN, INT16_MAX)
it_integer_add_implement_for_signed_type(int32_t, INT32_MIN, INT32_MAX)
it_integer_add_implement_for_signed_type(int64_t, INT64_MIN, INT64_MAX)

it_integer_add_sat_implement_for_signed_type(int8_t, INT8_MIN, INT8_MAX)
it_integer_add_sat_implement_for_signed_type(int16_t, INT16_MIN, INT16_MAX)
it_integer_add_sat_implement_for_signed_type(int32_t, INT32_MIN, INT32_MAX)
it_integer_add_sat_implement_for_signed_type(int64_t, INT64_MIN, INT64_MAX)

it_integer_sub_implement_for_signed_type(int8_t, INT8_MIN, INT8_MAX)
it_integer_sub_implement_for_signed_type(int16_t, INT16_MIN, INT16_MAX)
it_integer_sub_implement_for_signed_type(int32_t, INT32_MIN, INT32_MAX)
it_integer_sub_implement_for_signed_type(int64_t, INT64_MIN, INT64_MAX)

it_integer_mul_implement_for_signed_type(int8_t, INT8_MIN, INT8_MAX)
it_integer_mul_implement_for_signed_type(int16_t, INT16_MIN, INT16_MAX)
it_integer_mul_implement_for_signed_type(int32_t, INT32_MIN, INT32_MAX)
it_integer_mul_implement_for_signed_type(int64_t, INT64_MIN, INT64_MAX)

it_integer_div_implement_for_signed_type(int8_t, INT8_MIN, INT8_MAX)
it_integer_div_implement_for_signed_type(int16_t, INT16_MIN, INT16_MAX)
it_integer_div_implement_for_signed_type(int32_t, INT32_MIN, INT32_MAX)
it_integer_div_implement_for_signed_type(int64_t, INT64_MIN, INT64_MAX)

it_integer_add_implement_for_unsigned_type(uint8_t, UINT8_MAX)
it_integer_add_implement_for_unsigned_type(uint16_t, UINT16_MAX)
it_integer_add_implement_for_unsigned_type(uint32_t, UINT32_MAX)
it_integer_add_implement_for_unsigned_type(uint64_t, UINT64_MAX)

it_integer_add_sat_implement_for_unsigned_type(uint8_t, UINT8_MAX)
it_integer_add_sat_implement_for_unsigned_type(uint16_t, UINT16_MAX)
it_integer_add_sat_implement_for_unsigned_type(uint32_t, UINT32_MAX)
it_integer_add_sat_implement_for_unsigned_type(uint64_t, UINT64_MAX)

it_integer_sub_implement_for_unsigned_type(uint8_t, UINT8_MAX)
it_integer_sub_implement_for_unsigned_type(uint16_t, UINT16_MAX)
it_integer_sub_implement_for_unsigned_type(uint32_t, UINT32_MAX)
it_integer_sub_implement_for_unsigned_type(uint64_t, UINT64_MAX)

it_integer_mul_implement_for_unsigned_type(uint8_t, UINT8_MAX)
it_integer_mul_implement_for_unsigned_type(uint16_t, UINT16_MAX)
it_integer_mul_implement_for_unsigned_type(uint32_t, UINT32_MAX)
it_integer_mul_implement_for_unsigned_type(uint64_t, UINT64_MAX)

it_integer_div_implement_for_unsigned_type(uint8_t, UINT8_MAX)
it_integer_div_implement_for_unsigned_type(uint16_t, UINT16_MAX)
it_integer_div_implement_for_unsigned_type(uint32_t, UINT32_MAX)
it_integer_div_implement_for_unsigned_type(uint64_t, UINT64_MAX)

it_integer_clamp_implement_for_type(int8_t)
it_integer_clamp_implement_for_type(int16_t)
it_integer_clamp_implement_for_type(int32_t)
it_integer_clamp_implement_for_type(int64_t)
it_integer_clamp_implement_for_type(uint8_t)
it_integer_clamp_implement_for_type(uint16_t)
it_integer_clamp_implement_for_type(uint32_t)
it_integer_clamp_implement_for_type(uint64_t)

// Signed narrowing conversions:
it_integer_convert_implement_from_type_to_type(int64_t, INT64_MIN, INT64_MAX, int32_t, INT32_MIN, INT32_MAX)
it_integer_convert_implement_from_type_to_type(int64_t, INT64_MIN, INT64_MAX, int16_t, INT16_MIN, INT16_MAX)
it_integer_convert_implement_from_type_to_type(int64_t, INT64_MIN, INT64_MAX, int8_t, INT8_MIN, INT8_MAX)
it_integer_convert_implement_from_type_to_type(int32_t, INT32_MIN, INT32_MAX, int16_t, INT16_MIN, INT16_MAX)
it_integer_convert_implement_from_type_to_type(int32_t, INT32_MIN, INT32_MAX, int8_t, INT8_MIN, INT8_MAX)
it_integer_convert_implement_from_type_to_type(int16_t, INT16_MIN, INT16_MAX, int8_t, INT8_MIN, INT8_MAX)

// Unsigned narrowing conversions:
it_integer_convert_implement_from_type_to_type(uint64_t, 0, UINT64_MAX, uint32_t, 0, UINT32_MAX)
it_integer_convert_implement_from_type_to_type(uint64_t, 0, UINT64_MAX, uint16_t, 0, UINT16_MAX)
it_integer_convert_implement_from_type_to_type(uint64_t, 0, UINT64_MAX, uint8_t, 0, UINT8_MAX)
it_integer_convert_implement_from_type_to_type(uint32_t, 0, UINT32_MAX, uint16_t, 0, UINT16_MAX)
it_integer_convert_implement_from_type_to_type(uint32_t, 0, UINT32_MAX, uint8_t, 0, UINT8_MAX)
it_integer_convert_implement_from_type_to_type(uint16_t, 0, UINT16_MAX, uint8_t, 0, UINT8_MAX)

// Unsigned to signed narrowing conversions:
it_integer_convert_implement_from_type_to_type(uint64_t, 0, UINT64_MAX, int32_t, INT32_MIN, INT32_MAX)
it_integer_convert_implement_from_type_to_type(uint64_t, 0, UINT64_MAX, int16_t, INT16_MIN, INT16_MAX)
it_integer_convert_implement_from_type_to_type(uint64_t, 0, UINT64_MAX, int8_t, INT8_MIN, INT8_MAX)
it_integer_convert_implement_from_type_to_type(uint32_t, 0, UINT32_MAX, int16_t, INT16_MIN, INT16_MAX)
it_integer_convert_implement_from_type_to_type(uint32_t, 0, UINT32_MAX, int8_t, INT8_MIN, INT8_MAX)
it_integer_convert_implement_from_type_to_type(uint16_t, 0, UINT16_MAX, int8_t, INT8_MIN, INT8_MAX)

// Signed to unsigned narrowing conversions:
it_integer_convert_implement_from_type_to_type(int64_t, INT64_MIN, INT64_MAX, uint32_t, 0, UINT32_MAX)
it_integer_convert_implement_from_type_to_type(int64_t, INT64_MIN, INT64_MAX, uint16_t, 0, UINT16_MAX)
it_integer_convert_implement_from_type_to_type(int64_t, INT64_MIN, INT64_MAX, uint8_t, 0, UINT8_MAX)
it_integer_convert_implement_from_type_to_type(int32_t, INT32_MIN, INT32_MAX, uint16_t, 0, UINT16_MAX)
it_integer_convert_implement_from_type_to_type(int32_t, INT32_MIN, INT32_MAX, uint8_t, 0, UINT8_MAX)
it_integer_convert_implement_from_type_to_type(int16_t, INT16_MIN, INT16_MAX, uint8_t, 0, UINT8_MAX)

// Signed to unsigned:
it_integer_convert_implement_from_type_to_type(int64_t, INT64_MIN, INT64_MAX, uint64_t, 0, UINT64_MAX)
it_integer_convert_implement_from_type_to_type(int32_t, INT32_MIN, INT32_MAX, uint32_t, 0, UINT32_MAX)
it_integer_convert_implement_from_type_to_type(int16_t, INT16_MIN, INT16_MAX, uint16_t, 0, UINT16_MAX)
it_integer_convert_implement_from_type_to_type(int8_t, INT8_MIN, INT8_MAX, uint8_t, 0, UINT8_MAX)

// Unsigned to signed:
it_integer_convert_implement_from_type_to_type(uint64_t, 0, UINT64_MAX, int64_t, INT64_MIN, INT64_MAX)
it_integer_convert_implement_from_type_to_type(uint32_t, 0, UINT32_MAX, int32_t, INT32_MIN, INT32_MAX)
it_integer_convert_implement_from_type_to_type(uint16_t, 0, UINT16_MAX, int16_t, INT16_MIN, INT16_MAX)
it_integer_convert_implement_from_type_to_type(uint8_t, 0, UINT8_MAX, int8_t, INT8_MIN, INT8_MAX)

// Signed to unsigned widening conversions:
it_integer_convert_implement_from_type_to_type(int32_t, INT32_MIN, INT32_MAX, uint64_t, 0, UINT64_MAX)
it_integer_convert_implement_from_type_to_type(int16_t, INT16_MIN, INT16_MAX, uint64_t, 0, UINT64_MAX)
it_integer_convert_implement_from_type_to_type(int16_t, INT16_MIN, INT16_MAX, uint32_t, 0, UINT32_MAX)
it_integer_convert_implement_from_type_to_type(int8_t, INT8_MIN, INT8_MAX, uint64_t, 0, UINT64_MAX)
it_integer_convert_implement_from_type_to_type(int8_t, INT8_MIN, INT8_MAX, uint32_t, 0, UINT32_MAX)
it_integer_convert_implement_from_type_to_type(int8_t, INT8_MIN, INT8_MAX, uint16_t, 0, UINT16_MAX)

// Unsigned to signed widening conversions:
it_integer_convert_implement_from_type_to_type(uint32_t, 0, UINT32_MAX, int64_t, INT64_MIN, INT64_MAX)
it_integer_convert_implement_from_type_to_type(uint16_t, 0, UINT16_MAX, int64_t, INT64_MIN, INT64_MAX)
it_integer_convert_implement_from_type_to_type(uint16_t, 0, UINT16_MAX, int32_t, INT32_MIN, INT32_MAX)
it_integer_convert_implement_from_type_to_type(uint8_t, 0, UINT8_MAX, int64_t, INT64_MIN, INT64_MAX)
it_integer_convert_implement_from_type_to_type(uint8_t, 0, UINT8_MAX, int32_t, INT32_MIN, INT32_MAX)
it_integer_convert_implement_from_type_to_type(uint8_t, 0, UINT8_MAX, int16_t, INT16_MIN, INT16_MAX)

// Noop widening and same type conversions:
#define it_int64_t_from_int64_t(p_from) (p_from)
#define it_int64_t_from_int32_t(p_from) (int64_t)(p_from)
#define it_int64_t_from_int16_t(p_from) (int64_t)(p_from)
#define it_int64_t_from_int8_t(p_from) (int64_t)(p_from)
#define it_int32_t_from_int32_t(p_from) (p_from)
#define it_int32_t_from_int16_t(p_from) (int32_t)(p_from)
#define it_int32_t_from_int8_t(p_from) (int32_t)(p_from)
#define it_int16_t_from_int16_t(p_from) (p_from)
#define it_int16_t_from_int8_t(p_from) (int16_t)(p_from)
#define it_int8_t_from_int8_t(p_from) (p_from)

#define it_uint64_t_from_uint64_t(p_from) (p_from)
#define it_uint64_t_from_uint32_t(p_from) (uint64_t)(p_from)
#define it_uint64_t_from_uint16_t(p_from) (uint64_t)(p_from)
#define it_uint64_t_from_uint8_t(p_from) (uint64_t)(p_from)
#define it_uint32_t_from_uint32_t(p_from) (p_from)
#define it_uint32_t_from_uint16_t(p_from) (uint32_t)(p_from)
#define it_uint32_t_from_uint8_t(p_from) (uint32_t)(p_from)
#define it_uint16_t_from_uint16_t(p_from) (p_from)
#define it_uint16_t_from_uint8_t(p_from) (uint16_t)(p_from)
#define it_uint8_t_from_uint8_t(p_from) (p_from)

// clang-format on

#undef it_integer_add_implement_for_signed_type
#undef it_integer_sub_implement_for_signed_type
#undef it_integer_mul_implement_for_signed_type
#undef it_integer_div_implement_for_signed_type
#undef it_integer_add_implement_for_unsigned_type
#undef it_integer_sub_implement_for_unsigned_type
#undef it_integer_mul_implement_for_unsigned_type
#undef it_integer_div_implement_for_unsigned_type
#undef it_integer_clamp_implement_for_type
#undef it_integer_convert_implement_for_type

#undef IT_INTEGER_OPS_COMPILE
#undef IT_INTEGER_OPS_ATTRIBUTES
#undef it_integer_ops_trap_
