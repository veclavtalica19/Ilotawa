#pragma once

#include <ilotawa/include/ilotawa.h>

#include <tomolipu/include/compiler.h>
#include <tomolipu/include/types/vector2.h>

enum it_direction {
    it_direction_none,
    it_direction_right,
    it_direction_right_down,
    it_direction_down,
    it_direction_left_down,
    it_direction_left,
    it_direction_left_up,
    it_direction_up,
    it_direction_right_up,
    it_direction_count
};

IT_API IT_PURE enum it_direction it_direction_from_action_state(it_action_state const *state);

IT_API IT_PURE it_vector2_fixed it_direction_to_vector(enum it_direction);
