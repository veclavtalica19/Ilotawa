/**
 * @brief      Decentralized drawing queue that allows redraw optimization.
 *
 * @author     Veclav Talica
 * @date       2023
 *
 */

#pragma once

#include <stdint.h>

#include <ilotawa/include/types/color.h>
#include <ilotawa/include/types/texture.h>
#include <tomolipu/include/compiler.h>
#include <tomolipu/include/reflection.h>
#include <tomolipu/include/types/fixed.h>

typedef uint16_t it_drawing_queue_command_id;

typedef void it_drawing_queue_callback(void const *data);

IT_INTERNAL it_drawing_queue_command_id it_drawing_queue_register_command(
    it_typeinfo data_typeinfo, it_drawing_queue_callback *callback);

IT_INTERNAL bool it_drawing_queue_push_command(
    it_drawing_queue_command_id command, void const *data);

IT_INTERNAL bool it_drawing_queue_push_deferred_command(
    it_drawing_queue_command_id command, void const *data);

// @brief Flushes all stored commands no matter what, invalidating caches.
//
IT_INTERNAL void it_drawing_queue_invalidate(void);

// @brief Finalize frame, returns true if callbacks were executed.
//
IT_INTERNAL bool it_drawing_queue_flush(void);

IT_INTERNAL bool it_drawing_queue_module_initialize(void);

IT_INTERNAL void it_drawing_queue_module_deinitialize(void);

IT_INTERNAL void it_drawing_queue_module_clean(void);

#define it_drawing_queue_get_module_descriptor()                                                   \
    (it_module_descriptor) {                                                                       \
        .initializer = it_drawing_queue_module_initialize,                                         \
        .deinitializer = it_drawing_queue_module_deinitialize,                                     \
        .cleaner = it_drawing_queue_module_clean                                                   \
    }
