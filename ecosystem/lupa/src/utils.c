#include <stdbool.h>

#include <SDL2/SDL_stdinc.h>

#include <ilotawa/include/private/ilotawa.h>
#include <tomolipu/include/interfaces/math.h>

#include <lupa/include/target.h>
#include <lupa/include/utils.h>

#if defined(IT_PLATFORM_WINDOWS)
#include <lupa/include/platform/windows/api.h>
#endif

// todo: Highly dangerous procedure. More care needed.
size_t it_cstring_length(char const *cstring, size_t max_length) {
    it_ensure(cstring != NULL);
    if (max_length == 0)
        max_length = 4096;

#if defined(IT_PLATFORM_WINDOWS)
    UINT uint_max_length = it_convert(UINT, max_length);
    it_ensure(IsBadStringPtrA(cstring, uint_max_length) == 0);
#else

    // todo: Use memchr when libc is around.
    bool termination_found = false;
    for (size_t i = 0; i < max_length; ++i) {
        if (cstring[i] == 0) {
            termination_found = true;
            break;
        }
    }
    it_ensure(termination_found);

#endif // defined(IT_PLATFORM_WINDOWS)
    return SDL_strlen(cstring);
}
