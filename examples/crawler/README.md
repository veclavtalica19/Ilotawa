
# Licensing
## res/textures/floor_tiles.gif
https://opengameart.org/node/108812

[CC-BY 4.0](https://creativecommons.org/licenses/by/4.0/)

Created by Jordan Irwin (AntumDeluge)

## res/textures/entity_atlas.gif
### knight
[CC-BY-SA 3.0](http://creativecommons.org/licenses/by-sa/3.0/)
Made by The Pixelboy. Additionally, please include a link to my OGA profile. (opengameart.org/users/the-pixelboy)
