/**
 * @brief      Option convention.
 *
 * @author     Veclav Talica
 * @date       2022
 */

// todo: Stricter enforcement by requiring 'option' to have distinct type incompatible with plain
//       bool.

#define it_option_init(...)                                                                        \
    {                                                                                              \
        .option = true, .value = { __VA_ARGS__ }                                                   \
    }

#define it_option_from(p_value)                                                                    \
    { .option = true, .value = (p_value) }

// @warn Given optional shouldn't side effect on evaluation, as it's going to be expanded twice.
//
#define it_option_value_or(p_optional, p_default_value)                                            \
    ((p_optional).option ? (p_optional).value : (p_default_value))

// @warn Given optional shouldn't side effect on evaluation, as it's going to be expanded twice.
//
#define it_option_move(p_optional)                                                                 \
    { .option = (p_optional).option, .value = (p_optional).value }

#define it_option_set(p_optional_pointer, p_value)                                                 \
    {                                                                                              \
        (p_optional_pointer)->value = p_value;                                                     \
        (p_optional_pointer)->option = true;                                                       \
    }

#define it_option_set_unspecified(p_optional_pointer, p_value)                                     \
    {                                                                                              \
        if (not((p_optional_pointer)->option))                                                     \
            it_option_set(p_optional_pointer, p_value);                                            \
    }
