#pragma once

#include <tomolipu/include/compiler.h>
#include <tomolipu/include/types/buffer.h>

// todo: SSO

// @brief Utf-8 ensured string.
//
// todo: Cache utf-8 codepoint count.
//
typedef struct it_string {
    it_buffer buffer;
} it_string;

typedef struct it_string_iterator {
    uint32_t current; /* Utf32 rune */
    unsigned char *current_byte;
    unsigned char *end_byte;
} it_string_iterator;

// @brief Form string from C string literal. Only ansi and unicode literals are supported, wide
// literals are not.
//
#define it_string_from_literal(literal)                                                            \
    it_string_from_utf8((it_ensure(it_compare_types(literal, char[])),                             \
        it_buffer_from_sequence(                                                                   \
            literal, sizeof(literal) - 1, it_typeinfo_init(char), it_flag_access_readable)))

IT_API IT_PURE it_string it_string_from_utf8(it_buffer utf8_buffer);

// todo: uint32_t it_string_get(it_string const* string, uint32_t index);

IT_API IT_PURE bool it_string_check_equality(
    it_string const *restrict left, it_string const *restrict right);

IT_API void it_string_free(it_string *item);

// @brief Iterate over runes in utf8 string.
// @warning String should have readable flag.
//
IT_API IT_PURE it_string_iterator it_string_iterator_init(it_string const *string);

IT_API bool it_string_iterator_pump(it_string_iterator *it);
