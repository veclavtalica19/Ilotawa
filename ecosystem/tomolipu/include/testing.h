/**
 * @brief      Assertions beyond inline.
 *
 * @author     Veclav Talica
 * @date       2023
 */

#pragma once

#include <stdbool.h>
#include <stdint.h>

#include <tomolipu/include/compiler.h>

/* @brief Asserts for expression no matter what.
 */
IT_API void it_ensure_expression(
    bool status, char const *filename, uint32_t line, char const *expression);

#define it_ensure(expr) it_ensure_expression(it_likely(expr), __FILE__, __LINE__, #expr)

/* @brief Asserts for expression, but only when debugging definition is present.
 * @note  Depending on compiler support it will be assumed that expression holds true,
 *        providing optimization opportunities.
 */
#if defined(IT_BUILD_DEBUG)
#define it_test(expr) it_ensure_expression(it_likely(expr), __FILE__, __LINE__, #expr)
#else
#define it_test(expr) ((void)it_assume(expr))
#endif
