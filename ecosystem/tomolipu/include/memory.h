#pragma once

// todo: Memory allocations should be able to be aligned to arbitrary boundary.

#include <stddef.h>
#include <stdint.h>

#include <tomolipu/include/compiler.h>
#include <tomolipu/include/reflection.h>

typedef struct it_memory_stats {
    size_t bytes_used;
    size_t max_bytes_used;
    size_t n_allocations;
    size_t n_reallocations;
    size_t n_frees;
} it_memory_stats;

// todo: Implement over 'it_buffer' instead.

IT_INTERNAL void it_memory_set_byte_sequence(void *result, size_t result_size, uint8_t value);

IT_INTERNAL void it_memory_copy_byte_sequence(
    void *restrict result, size_t result_size, void const *restrict source, size_t source_size);

IT_INTERNAL IT_PURE bool it_memory_are_byte_sequences_identical(
    void const *restrict left, size_t left_size, void const *restrict right, size_t right_size);

IT_INTERNAL IT_PURE bool it_memory_is_byte_sequence_equal_to(
    void const *restrict sequence, size_t bytes, uint8_t value);

IT_INTERNAL bool it_memory_manage_allocation(
    void **result, it_typeinfo typeinfo, size_t old_length, size_t new_length);

IT_INTERNAL void *it_memory_realloc_adapter(void *result, size_t bytes);

IT_INTERNAL it_memory_stats it_memory_get_stats(void);
