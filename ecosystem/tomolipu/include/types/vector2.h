/**
 * @brief      Generic two component vectors.
 *
 * @author     Veclav Talica
 * @date       2023
 *
 */

#pragma once

#include <stdint.h>

#include <tomolipu/include/compiler.h>
#include <tomolipu/include/types/fixed.h>

// todo: The rest of math operations.

#define it_vector2_implement_for_type(p_type, p_typename)                                          \
    typedef struct it_vector2_##p_typename {                                                       \
        union {                                                                                    \
            p_type x;                                                                              \
            p_type width;                                                                          \
        };                                                                                         \
                                                                                                   \
        union {                                                                                    \
            p_type y;                                                                              \
            p_type height;                                                                         \
        };                                                                                         \
    } it_vector2_##p_typename;

// clang-format off

it_vector2_implement_for_type(uint8_t, uint8_t)
it_vector2_implement_for_type(uint16_t, uint16_t)
it_vector2_implement_for_type(uint32_t, uint32_t)
it_vector2_implement_for_type(uint64_t, uint64_t)
it_vector2_implement_for_type(int8_t, int8_t)
it_vector2_implement_for_type(int16_t, int16_t)
it_vector2_implement_for_type(int32_t, int32_t)
it_vector2_implement_for_type(int64_t, int64_t)
it_vector2_implement_for_type(it_fixed, fixed)

// clang-format on

#if defined(IT_VECTOR2_INLINED)

#include <tomolipu/include/testing.h>

#define IT_VECTOR2_COMPILE
#define IT_VECTOR2_ATTRIBUTES static inline
#define it_vector2_trap_      it_test

#endif // !defined(IT_VECTOR2_INLINED)

#include <tomolipu/include/types/private/vector2_implementation.h>
