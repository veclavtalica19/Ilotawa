#pragma once

#include <stdint.h>

#include <ilotawa/include/types/atlas.h>
#include <ilotawa/include/types/color.h>
#include <tomolipu/include/compiler.h>
#include <tomolipu/include/interfaces/option.h>
#include <tomolipu/include/types/fixed.h>
#include <tomolipu/include/types/string.h>

enum it_font_kind {
    it_font_kind_unkown,
    it_font_kind_bitmap,
};

// @brief Adapter for different types of fonts.
//
typedef struct it_font {
    enum it_font_kind kind;

    union {
        struct {
            it_atlas atlas;
        } bitmap;
    };
} it_font;

// todo: Use it_atlas_draw_options for atlas draw shared optons.
typedef struct it_font_draw_options {
    // @default 1.0
    struct {
        it_fixed value;
        bool option;
    } scaling;

    struct {
        uint16_t value;
        bool option;
    } limit;

    // @default (255, 255, 255, 255)
    struct {
        it_color value;
        bool option;
    } color;
} it_font_draw_options;

// @brief Use grid bitmap as font.
//
// @note Upper-left most tile should correspond to U+0020.
//
IT_API bool it_font_from_bitmap(
    it_font *result, it_texture texture, uint16_t tile_width, uint16_t tile_height);

// @brief Draw font object on the screen.
//
IT_API void it_font_draw(it_font const *font,
    it_vector2_fixed position,
    it_string const *string,
    it_font_draw_options const *options);

// todo:
// it_extends void it_font_draw_extends(it_font const *font, it_string const *string,
// it_font_draw_options *options);

IT_API void it_font_free(it_font *font);
