#pragma once

#include <ilotawa/include/types/image.h>
#include <ilotawa/include/types/texture.h>
#include <tomolipu/include/compiler.h>
#include <tomolipu/include/types/buffer.h>

IT_API bool it_buffer_from_file(it_buffer *result, char const *filepath);

IT_API bool it_image_from_buffer(it_image *result, it_buffer buffer);

IT_API bool it_texture_from_file(it_texture *result, char const *filepath, bool with_transparency);
