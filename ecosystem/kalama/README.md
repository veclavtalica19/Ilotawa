# kalama
Various procedural generation utilities.

## Interface
Generally two forms are defined for each method: `_bulk` and non-bulk

Bulk ones operate on all data at once for efficiency, while non-bulk provide feedback mechanism for scenarios where upfront allocations are not desirable.
