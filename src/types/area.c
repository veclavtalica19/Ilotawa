#define IT_INTEGER_OPS_INLINED
#define IT_VECTOR2_INLINED
#define IT_FIXED_INLINED
#include <ilotawa/include/types/area.h>
#include <tomolipu/include/compiler.h>
#include <tomolipu/include/interfaces/math.h>
#include <tomolipu/include/testing.h>
#include <tomolipu/include/types/fixed.h>
#include <tomolipu/include/types/vector2.h>

it_vector2_fixed it_area_point_at_origin(it_area area, enum it_origin origin) {
    switch (origin) {
    case it_origin_center:
        return it_vector2_fixed_add(area.origin,
            it_vector2_fixed_div(area.extend,
                (it_vector2_fixed) { .x = it_fixed_init(it_sign_plus, 2, 0),
                    .y = it_fixed_init(it_sign_plus, 2, 0) }));
    case it_origin_upperleft:
        return area.origin;
    default:
        it_ensure(false);
    }
    it_unreachable();
    return (it_vector2_fixed) { 0 };
}
