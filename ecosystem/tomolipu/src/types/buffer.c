#include <iso646.h>
#include <stdbool.h>
#include <stdint.h>

#define IT_INTEGER_OPS_INLINED
#include <tomolipu/include/compiler.h>
#include <tomolipu/include/interfaces/math.h>
#include <tomolipu/include/memory.h>
#include <tomolipu/include/signal.h>
#include <tomolipu/include/testing.h>
#include <tomolipu/include/types/buffer.h>
#include <tomolipu/include/types/private/buffer.h>

// todo: Use ensure rather than test.
// todo: Currently it's assumed that data is tightly packed, but alignment requirements might break
//       it.

#define is_well_formed(p_buffer)                                                                   \
    (((p_buffer)) and ((p_buffer)->private.length <= (p_buffer)->private.capacity)                 \
        and (((p_buffer)->private.capacity == 0 and (not(p_buffer)->private.data))                 \
            or ((p_buffer)->private.data                                                           \
                and it_is_aligned(                                                                 \
                    (p_buffer)->private.data, (p_buffer)->private.typeinfo.alignment)              \
                and (it_is_power_of_two((p_buffer)->private.typeinfo.alignment))                   \
                and ((p_buffer)->private.typeinfo.size != 0                                        \
                    and (p_buffer)->private.typeinfo.alignment != 0))))

it_buffer it_buffer_init(it_typeinfo const typeinfo) {
    it_buffer result = { .private = {
                             .typeinfo = typeinfo,
                             .flags = it_flag_access_writable bitor it_flag_access_readable,
                         } };

    it_test(is_well_formed(&result));
    return result;
}

it_buffer it_buffer_from_sequence(void *const sequence,
    uint32_t const sequence_length,
    it_typeinfo const sequence_typeinfo,
    enum it_flag const flags) {
    it_buffer result = { .private = {
                             .data = sequence,
                             .length = sequence_length,
                             .capacity = sequence_length,
                             .typeinfo = sequence_typeinfo,
                             .flags = flags bitor it_flag_static,
                         } };

    it_test(is_well_formed(&result));
    return result;
}

uint32_t it_buffer_get_length(it_buffer const *const buffer) {
    it_test(is_well_formed(buffer));

    return buffer->private.length;
}

it_typeinfo it_buffer_get_typeinfo(it_buffer const *const buffer) {
    it_test(is_well_formed(buffer));

    return buffer->private.typeinfo;
}

enum it_flag it_buffer_get_flags(it_buffer const *const buffer) {
    it_test(is_well_formed(buffer));

    return buffer->private.flags;
}

bool it_buffer_ensure_capacity(it_buffer *const buffer, uint32_t const capacity) {
    it_test(is_well_formed(buffer));

    if (buffer->private.capacity >= capacity)
        return true;

    if (buffer->private.flags bitand it_flag_static)
        return false;

    // First allocation is always of exact capacity to accommodate cases where all needed memory is
    // allocated in advance.
    uint32_t const overcapacity
        = buffer->private.capacity == 0 ? capacity : it_add_sat(uint32_t, capacity, capacity / 3);

    if (not it_memory_manage_allocation(&buffer->private.data, buffer->private.typeinfo,
            buffer->private.capacity, overcapacity)) {
        // If greedy capacity fails - try to use exact byte count.
        if (not it_memory_manage_allocation(&buffer->private.data, buffer->private.typeinfo,
                buffer->private.capacity, capacity)) {
            // Attempt to free memory via cleanup callbacks.
            it_signals_invoke(it_signal_clean);

            if (not it_memory_manage_allocation(&buffer->private.data, buffer->private.typeinfo,
                    buffer->private.capacity, capacity))
                return false;
        }

        buffer->private.capacity = capacity;

    } else
        buffer->private.capacity = overcapacity;

    it_test(it_is_aligned(buffer->private.data, buffer->private.typeinfo.alignment));
    return true;
}

bool it_buffer_grow_by_item_count(it_buffer *const buffer, uint32_t const item_count) {
    it_test(buffer != NULL);

    if (not it_buffer_ensure_capacity(buffer, it_add(uint32_t, buffer->private.length, item_count)))
        return false;

    it_memory_set_byte_sequence(
        (void *)it_add(uintptr_t, (uintptr_t)buffer->private.data,
            it_mul(uintptr_t, buffer->private.length, buffer->private.typeinfo.size)),
        (size_t)item_count * buffer->private.typeinfo.size, 0);

    buffer->private.length += item_count;
    return true;
}

bool it_buffer_grow_to_length(it_buffer *const buffer, uint32_t const length) {
    it_test(buffer and (length >= buffer->private.length));

    return it_buffer_grow_by_item_count(buffer, length - buffer->private.length);
}

bool it_buffer_push(it_buffer *const restrict buffer, void const *const restrict item) {
    it_test(is_well_formed(buffer) and (item)
        and (buffer->private.flags bitand it_flag_access_writable)
        and (it_is_aligned(item, buffer->private.typeinfo.alignment)));

    if (not it_buffer_ensure_capacity(buffer, it_inc(buffer->private.length)))
        return false;

    it_memory_copy_byte_sequence(
        (void *)it_add(uintptr_t, (uintptr_t)buffer->private.data,
            it_mul(uintptr_t, buffer->private.length, buffer->private.typeinfo.size)),
        buffer->private.typeinfo.size, item, buffer->private.typeinfo.size);

    buffer->private.length++;

    return true;
}

void it_buffer_set(
    it_buffer const *const restrict buffer, uint32_t const index, void const *const restrict item) {
    it_test(is_well_formed(buffer) and (item) and (buffer->private.length > index)
        and (buffer->private.flags bitand it_flag_access_writable)
        and (it_is_aligned(item, buffer->private.typeinfo.alignment)));

    it_memory_copy_byte_sequence((void *)it_add(uintptr_t, (uintptr_t)buffer->private.data,
                                     it_mul(uintptr_t, index, buffer->private.typeinfo.size)),
        buffer->private.typeinfo.size, item, buffer->private.typeinfo.size);
}

void *it_buffer_get(it_buffer const *const buffer, uint32_t const index) {
    it_test(is_well_formed(buffer) and (buffer->private.length > index)
        and (buffer->private.flags bitand it_flag_access_readable));

    return (void *)it_add(uintptr_t, (uintptr_t)buffer->private.data,
        it_mul(uintptr_t, index, buffer->private.typeinfo.size));
}

bool it_buffer_append(it_buffer *const buffer, it_buffer other) {
    it_test(is_well_formed(buffer) and (is_well_formed(&other))
        and (other.private.length <= other.private.capacity)
        and (buffer->private.length <= buffer->private.capacity)
        and (not other.private.data or other.private.capacity != 0)
        and (not buffer->private.data or buffer->private.capacity != 0)
        and (buffer->private.flags bitand it_flag_access_writable)
        and (buffer->private.typeinfo.size == other.private.typeinfo.size)
        and (buffer->private.typeinfo.alignment == other.private.typeinfo.alignment));

    if (not it_buffer_ensure_capacity(buffer, buffer->private.length + other.private.length)) {
        // todo: Faiure allocation forces argument to be lost currently, which is suboptimal.
        it_buffer_free(&other);
        return false;
    }

    it_memory_copy_byte_sequence(
        (void *)it_add(uintptr_t, (uintptr_t)buffer->private.data,
            it_mul(uintptr_t, buffer->private.length, buffer->private.typeinfo.size)),
        it_mul(size_t, other.private.length, other.private.typeinfo.size), other.private.data,
        it_mul(size_t, other.private.length, other.private.typeinfo.size));

    it_buffer_free(&other);
    return true;
}

void it_buffer_free(it_buffer *const buffer) {
    it_test(is_well_formed(buffer));

    if (buffer->private.data and (not(buffer->private.flags bitand it_flag_static))) {
        it_test(it_memory_manage_allocation(
            (void **)&buffer->private.data, buffer->private.typeinfo, buffer->private.capacity, 0));
        buffer->private.data = NULL;
        buffer->private.capacity = 0;
    }

    buffer->private.length = 0;
}

it_buffer it_buffer_make_view(it_buffer const *const buffer, enum it_flag const flags) {
    return it_buffer_make_subview(buffer, 0, buffer->private.length, flags);
}

it_buffer it_buffer_make_subview(it_buffer const *const buffer,
    uint32_t const start,
    uint32_t const length,
    enum it_flag const flags) {
    it_test(is_well_formed(buffer) and (start < buffer->private.length)
        and (length <= buffer->private.length)
        and (it_add(uint32_t, start, length) <= buffer->private.length)
        and ((buffer->private.flags bitand flags) == flags));

    void *const data = (void *)it_add(uintptr_t, (uintptr_t)buffer->private.data,
        it_mul(uintptr_t, start, buffer->private.typeinfo.size));

    return (it_buffer) { .private = { .data = data,
                             .length = length,
                             .capacity = length,
                             .typeinfo = buffer->private.typeinfo,
                             .flags = it_flag_static bitor flags } };
}

void it_buffer_shrink(it_buffer *const buffer) {
    it_test(is_well_formed(buffer));

    if (buffer->private.flags bitand it_flag_static)
        return;

    it_ensure(it_memory_manage_allocation((void **)&buffer->private.data, buffer->private.typeinfo,
        buffer->private.capacity, buffer->private.length));
    buffer->private.capacity = buffer->private.length;
}

bool it_buffer_check_equality(
    it_buffer const *const restrict buffer, it_buffer const *const restrict other) {
    it_test(is_well_formed(buffer) and (is_well_formed(other)));

    return it_memory_are_byte_sequences_identical(
        buffer->private.data, buffer->private.length, other->private.data, other->private.length);
}

bool it_buffer_is_initialized(it_buffer const *buffer) {
    it_test(buffer != NULL);

    return !it_memory_is_byte_sequence_equal_to(buffer, sizeof(it_buffer), 0);
}

// todo: Allow arbitrary memory addresses used.
it_buffer_iterator it_buffer_iterator_init(
    it_buffer const *const buffer, enum it_flag const flags) {
    it_test(is_well_formed(buffer) and ((buffer->private.flags bitand flags) == flags));

    return (it_buffer_iterator) { .current = buffer->private.length != 0
            ? (char *)it_sub(
                uintptr_t, (uintptr_t)buffer->private.data, buffer->private.typeinfo.size)
            : NULL,
        .private = { .typeinfo = buffer->private.typeinfo, .left = buffer->private.length } };
}

bool it_buffer_iterator_pump(it_buffer_iterator *const it) {
    if (it->private.left == 0)
        return false;

    it->private.left--;

    // todo: Should be overflow checked.
    it->current = &((char *)it->current)[it->private.typeinfo.size];

    return true;
}

void *it_buffer_get_end_pointer(it_buffer const *const buffer) {
    it_test(is_well_formed(buffer));

    // It should not return NULL in such case.
    it_ensure(buffer->private.data and buffer->private.length != 0);

    return (void *)it_add(uintptr_t, (uintptr_t)buffer->private.data,
        it_mul(uintptr_t, (uintptr_t)buffer->private.length, buffer->private.typeinfo.size));
}
