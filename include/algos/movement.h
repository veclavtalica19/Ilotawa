#pragma once

#include <ilotawa/include/ilotawa.h>
#include <ilotawa/include/types/direction.h>

#include <tomolipu/include/compiler.h>

IT_API IT_PURE it_vector2_fixed it_movement_8way_step(
    it_vector2_fixed position, enum it_direction direction, it_fixed distance);
