#include <iso646.h>
#include <stdbool.h>

#include <SDL2/SDL_stdinc.h>

#define IT_INTEGER_OPS_INLINED
#include <tomolipu/include/interfaces/math.h>
#include <tomolipu/include/memory.h>
#include <tomolipu/include/private/memory.h>
#include <tomolipu/include/testing.h>

IT_INTERNAL void it_memory_set_byte_sequence(
    void *const result, size_t const result_size, uint8_t const value) {
    it_test(result != NULL);

    SDL_memset(result, value, result_size);
}

void it_memory_copy_byte_sequence(void *restrict const result,
    size_t const result_size,
    void const *restrict const source,
    size_t const source_size) {
    it_test(result and source);
    it_test(result_size >= source_size);

    SDL_memcpy(result, source, result_size);
}

bool it_memory_are_byte_sequences_identical(void const *restrict const left,
    size_t const left_size,
    void const *restrict const right,
    size_t const right_size) {
    it_test(left and right);

    if (left_size != right_size)
        return false;

    return SDL_memcmp(left, right, left_size) == 0;
}

bool it_memory_manage_allocation(void **const result,
    it_typeinfo const typeinfo,
    size_t const old_length,
    size_t const new_length) {
    it_test(result != NULL);
    it_test((*result and old_length != 0) or (not *result and old_length == 0 and new_length != 0));

    // SDL2 has no alignmented allocator.
    it_ensure(typeinfo.size >= typeinfo.alignment);

    if (new_length == old_length)
        return true;

    void *tmp = SDL_realloc(*result, it_mul(size_t, typeinfo.size, new_length));
    it_ensure(it_is_aligned(tmp, typeinfo.alignment));

    if (new_length == 0) {
        stats.n_frees++;
        stats.bytes_used -= it_mul(size_t, typeinfo.size, old_length);

    } else {
        if (not tmp)
            return false;

        // todo: Should it be performed? We might choose to only do it on debug.
        //       We only allocate memory via buffer and it can handle zeroing a lot smarter.
        if (new_length > old_length) {
            it_memory_set_byte_sequence((void *)it_add(uintptr_t, (uintptr_t)tmp,
                                            it_mul(uintptr_t, typeinfo.size, old_length)),
                it_mul(uintptr_t, typeinfo.size, new_length - old_length), 0);

            if (old_length == 0)
                stats.n_allocations++;
            else
                stats.n_reallocations++;

            stats.bytes_used += it_mul(size_t, typeinfo.size, new_length - old_length);

        } else {
            stats.n_reallocations++;
            stats.bytes_used -= it_mul(size_t, typeinfo.size, old_length - new_length);
        }
    }

    if (stats.bytes_used > stats.max_bytes_used)
        stats.max_bytes_used = stats.bytes_used;

    *result = tmp;
    return true;
}

// todo: Drive stats form here too.
void *it_memory_realloc_adapter(void *const result, size_t const bytes) {
    return SDL_realloc(result, bytes);
}
