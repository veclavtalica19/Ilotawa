# ilotawa
Simplistic multimedia runtime and its interface and ecosystem around it.

## Principles
- One single way of doing one single thing.
- Features are toxic. Optimizations are too. Become free from everything unless it's absolutely needed.
- Compilation should be easy. Better yet it should not exist, but we dont live in a perfect world.
- Consistency matters. Documentation should be needed as less as possible.
Internalizing the interface pattern should help with that.
- Convention matters. It allows for assumptions that are helpful for tooling, but also consistency.
- Incrementalism. Need i say more?
- Zero target specific code.
- Discreteness is better.
- The less customizable something is - the better.

## Interface conventions
- Everything non-C specific should be an extern non-inline function, for the sake of FFI.
- Variadic functions are forbidden, as they make FFI complex and in general it's a bad practice.
- Ownership is denoted by value/pointer passage.
Everything value is ought to be treated as move. Pointers leave the ownership at the same place.
On error ownership is returned to caller.
- Try to always build on top of something instead of repeating.
- Driving by callbacks is problematic, polling should be preferred instead.
- Use enum for declaring const values, it's the only way to do it without using preprocessor or having in-memory objects that are just read-only.
- Public types must be well-formed just by being zero initialized. They don't have to be meaningful, but should not cause panics either.
- If error is catchable - it should be ensured that no state was changed by function returning the error, if it cannot be proven - panic should ensue.

### Iterator
- `it_x_iterator` should a struct that holds the iteration context. it must have field named `current` that holds the current item.
- `it_x_iterator_init()` function should return initialized iterator context.
- `it_x_iterator_pump()` should progress the iteration and return bool value, depending on whether iteration is finished or not. `false` is returned to signal finish. Pumping past finish should always return false.
- Accessing `current` is only allowed after any call to `it_x_iterator_pump()`
- After finishing iteration access to `current` is invalid.
- Not finishing iteration fully is allowed, meaning no resources should be owned by iteration context.

### Option
Option is any struct that suffice given conditions:
- It should have exactly two fields.
- First field should be named `value`. and might have any type.
- Second field should be named `option` and have type `bool`.
- `value` is only needed to be well-formed in case `option` have value `true`, otherwise access to it is erroneous.

## Resources
- [:)](https://zrajm.github.io/toki-pona-syllabics/dictionary/)
- [Linux read on memory (un)alignment access](https://www.kernel.org/doc/Documentation/unaligned-memory-access.txt)
- [Bitfield ramble](https://www.flamingspork.com/blog/2014/11/14/c-bitfields-considered-harmful/)
- [CERT presentation about integer security](https://resources.sei.cmu.edu/asset_files/Presentation/2011_017_001_51345.pdf)
- [Why modulo-ing the RNG is not enough](https://stackoverflow.com/questions/10984974/why-do-people-say-there-is-modulo-bias-when-using-a-random-number-generator)
- [About designing randomness](https://www.redblobgames.com/articles/probability/damage-rolls.html)
- [About C integer types](https://embeddedgurus.com/stack-overflow/2008/06/efficient-c-tips-1-choosing-the-correct-integer-size/)
- [SEI CERT C coding standard](https://wiki.sei.cmu.edu/confluence/display/c/SEI+CERT+C+Coding+Standard)
- [Integer promotion rules are of special relevance](https://wiki.sei.cmu.edu/confluence/display/c/INT02-C.+Understand+integer+conversion+rules)
- [About inline functions](http://www.greenend.org.uk/rjk/tech/inline.html)
- [Behavior of macro expansion](https://gcc.gnu.org/onlinedocs/cpp/Argument-Prescan.html)
- [Varargs and wrapping](https://www.swig.org/Doc1.3/Varargs.html)
- [Client-server interpolation](https://www.gabrielgambetta.com/client-side-prediction-server-reconciliation.html)
- [C11 _Generic portability](https://gustedt.wordpress.com/2015/05/11/the-controlling-expression-of-_generic/)
- [Deep C stuff](https://blog.joren.ga/less-known-c)
- [Pointer shenanigans](https://pvs-studio.com/en/blog/posts/cpp/0576/)
- [About aliasing](https://developers.redhat.com/blog/2020/06/02/the-joys-and-perils-of-c-and-c-aliasing-part-1#objects_and_types_in_c_and_c__)

## Disclaimer
All files in this repo are subject to GNU General Public License 3 unless stated otherwise.
